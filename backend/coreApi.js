let sendInternalPayment = (fromClientId, toClientId, amount) => {
    /*
        This should send a HTTP request to the banking core application
     */
};

let sendExternalPayment = (receiverAccount, amount) => {
    /*
     This should send a HTTP request to the banking core application
     */
};

module.exports = { sendInternalPayment, sendExternalPayment };