const https = require('https');
const fbHelper = require('./fbHelper');

function getClientIdFromFacebookId(db, facebookId) {
    const getQuery = `SELECT * FROM users WHERE facebook_id = '${facebookId}'`;

    db.query(getQuery, (error, results, fields) => {
        if(error) {
            console.error(error);
            return null;
        } else if(results.length > 0) {
            return results[0].client_id;
        } else {
            return null;
        }
    });
};

function getReceivedPayments(db, facebookId) {
    return new Promise((resolve, reject) => {
        let result = [];

        const getQuery = `SELECT users.full_name as name, 
    users.image_url as image,
     payments.amount as amount,
     payments.status as status, 
     payments.receiver_data as data FROM users, payments WHERE users.facebook_id = payments.to_facebook_id AND payments.to_facebook_id = '${facebookId}'`;

        db.query(getQuery, (error, data, fields) => {
            if(error) {
                console.error(error);
                reject(error);
            } else {
                for(let i = 0; i < data.length; i++) {
                    result.push({
                        send: true,
                        senderName: data[i].name,
                        senderImage: data[i].image,
                        amount: data[i].amount,
                        status: data[i].status,
                        data: data[i].data})
                }
                return resolve(result);
            }
        });
    });
}

function getSentPayments(db, facebookId) {
    return new Promise((resolve, reject) => {
        let result = [];

        const getQuery = `SELECT users.full_name as name, 
    users.image_url as image,
     payments.amount as amount,
     payments.status as status, 
     payments.receiver_data as data FROM users, payments WHERE users.facebook_id = payments.from_facebook_id AND payments.from_facebook_id = '${facebookId}'`;

        db.query(getQuery, (error, data, fields) => {
            if(error) {
                console.error(error);
                reject(error);
            } else {
                for(let i = 0; i < data.length; i++) {
                    console.log(result);
                    result.push({
                        receive: true,
                        receiverName: data[i].name,
                        receiverImage: data[i].image,
                        amount: data[i].amount,
                        status: data[i].status,
                        data: data[i].data})
                }
                return resolve(result);
            }
        });

    })
}

function generatePaymentId() {
    let result = "";
    let allowedCharacters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for(var i = 0; i < 32; i++) {
        result += allowedCharacters.charAt(Math.floor(Math.random() * allowedCharacters.length));
    }

    return result;
};

function calculateLikeBoost(db, likeName, score, callback) {
    fbHelper.getUserLikes(db, function(likesData) {
        for(let i = 0; i < likesData.length; i++) {
            if(likesData[i].name == likeName) {
                return callback((score / likesData.length));
            }
        }

        return callback(0);
    });
}

module.exports = {
    getClientIdFromFacebookId,
    generatePaymentId,
    getReceivedPayments,
    getSentPayments,
    calculateLikeBoost
};