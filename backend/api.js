let mysql  = require('mysql'),
    config = require('./config').config,
    helper = require('./helper'),
    coreApi = require('./coreApi');

let connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'fintech'
});

const PAYMENT_STATUS_INTERNAL = "internal";
const PAYMENT_STATUS_WAITING_FOR_RESPONSE = "waiting";
const PAYMENT_STATUS_EXTERNAL = "external";
const PAYMENT_STATUS_RESPONDED = "responded";

connection.connect();

let setFacebookToken = (req, res) => {
    let { client_id, facebook_id, facebook_access_token, full_name, image_url } = req.body;
    console.log(req.body);

    const query = `INSERT INTO users(client_id, facebook_id, facebook_access_token, full_name, image_url) 
                    VALUES('${client_id}', '${facebook_id}', '${facebook_access_token}', '${full_name}', '${image_url}')`;

    connection.query(query, (error, user, fields) => {
        if(error) {
            console.error(error);
            return res.status(400).send({
                message: error.toString(),
                ok: false
            })
        };

        res.send({
            ok: true
        });
    });
};

let sendPayment = (req, res) => {
    let { from_facebook_id, to_facebook_id, amount } = req.body;

    let senderClientId = helper.getClientIdFromFacebookId(connection, from_facebook_id);
    let receiverClientId = helper.getClientIdFromFacebookId(connection, to_facebook_id);
    let paymentStatus = null;

    if(receiverClientId != null) {
        // Receiver is also banks client, send direct request
        coreApi.sendInternalPayment(senderClientId, receiverClientId, amount);
        paymentStatus = PAYMENT_STATUS_INTERNAL;
    } else {
        // Receiver is not banks client, we need to ask him how he would like to receive the payment
        paymentStatus = PAYMENT_STATUS_WAITING_FOR_RESPONSE;
    }

    let secretId = helper.generatePaymentId();

    const insertQuery = `INSERT INTO payments(secret_id, from_facebook_id, to_facebook_id, amount, status, receiver_data) 
                        VALUES('${secretId}', '${from_facebook_id}', '${to_facebook_id}', '${amount}', '${paymentStatus}', NULL)`;

    connection.query(insertQuery, (error) => {
        if(error) {
            console.error(error);
            return res.status(400).send({
                message: error.toString(),
                ok: false
            })
        };

        res.send({
            ok: true,
            secret_id: secretId,
            status: paymentStatus
        });
    });
};

let paymentResponse = (req, res) => {
    let { secret_id, payment_type, account } = req.body;

    if(payment_type == PAYMENT_STATUS_EXTERNAL) {
        coreApi.sendExternalPayment(account);
    }

    let receiverData = JSON.stringify({ payment_type: payment_type, account: account});

    const updateQuery = `UPDATE payments SET status = '${PAYMENT_STATUS_RESPONDED}', receiver_data = '${receiverData}' WHERE secret_id = '${secret_id}'`;

    connection.query(updateQuery, (error) => {
        if(error) {
            console.error(error);
            return res.status(400).send({
                message: error.toString(),
                ok: false
            });
        };

        res.send({
            ok: true
        });
    });
};

let getPayments = (req, res) => {
    let { facebook_id } = req.query;

    let result = {
        receivedPayments: [],
        sentPayments: []
    };
    Promise.all([
        helper.getSentPayments(connection, facebook_id),
        helper.getReceivedPayments(connection, facebook_id)
    ]).then(data => {
        res.status(200).send([...data[0], ...data[1]])
    });
};

let paymentInfo = (req, res) => {
    let { secret_id } = req.query;

    const getQuery = `SELECT users.full_name as name, 
    users.image_url as image,
     payments.amount as amount FROM users, payments WHERE users.facebook_id = payments.from_facebook_id AND payments.secret_id = '${secret_id}'`;

    connection.query(getQuery, (error, data, fields) => {
        if(error){
            return res.status(400).send(error);
        }

        res.status(200).send({
            name: data[0].name,
            image: data[0].image,
            amount: data[0].amount,

        });
    });
};

let addIndicator = (req, res) => {
    let { type, product, name, score } = req.query;

    const insertQuery = `INSERT INTO indicators(type, product, name, score) 
                        VALUES('${type}', '${product}', '${name}', '${score}')`;

    connection.query(insertQuery, (error) => {
        if(error) {
            console.error(error);
            return res.status(400).send({
                message: error.toString(),
                ok: false
            })
        };

        if(type == 'like') {
            helper.calculateLikeBoost(connection, name, score, function(scoreBoost) {
                res.status(200).send("" + scoreBoost);
            });
        } else if(type == 'life_event') {
            /*
             To be implemented...
             */
        } else if(type == 'joined_group') {
            /*
             To be implemented...
             */
        }
    });
};

let applyRoutes = (app) => {
    app.post('/setFacebookToken', setFacebookToken);
    app.post('/sendPayment', sendPayment);
    app.post('/paymentResponse', paymentResponse);
    app.get('/getPayments', getPayments);
    app.get('/paymentInfo', paymentInfo);
    app.get('/addIndicator', addIndicator);
};

exports.applyRoutes = applyRoutes;