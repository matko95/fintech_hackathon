const FB = require('fb');

function getUserLikes(db, callback) {
    const getQuery = `SELECT * FROM users`;

    db.query(getQuery, (error, results, fields) => {
        FB.setAccessToken(results[results.length-1].facebook_access_token);

        FB.api('/me/likes', function(response) {
            callback(response.data);
        });
    });
}

module.exports = { getUserLikes };