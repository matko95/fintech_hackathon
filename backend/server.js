let express = require('express'),
    config  = require('./config'),
    api     = require('./api'),
    coreApi = require('./coreApi'),
    path    = require('path'),
    server;

// Create the HTTP server (Express 3.0 version)
server = express();

// Apply the configuration
config.applyConfiguration(server);

// Add the api routes
api.applyRoutes(server);

server.use('/public', express.static(__dirname + '/public'));

server.get('/*', function(req, res){
    res.sendFile(path.resolve(__dirname + '/../public/index.html'));
});

// Export the server
module.exports = server;