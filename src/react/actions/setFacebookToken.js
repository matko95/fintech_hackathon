export const setFacebookToken = (fromID, accessToken, full_name, image_url) => {
    let config = {
        method: "POST",
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify({
            client_id: null,
            facebook_id: fromID,
            facebook_access_token: accessToken,
            full_name,
            image_url
        })
    };

    return dispatch => {
        return fetch(`/setFacebookToken`, config)
            .then(response => response.json()
            .then(data => ({ data, response })))
            .then(({ data, response }) =>  {
                if (!response.ok) {
                    return;
                }
            })
            .catch(err => console.log(err));
    }
};