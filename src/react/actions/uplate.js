export const uplate_request = () => {
    return {
        type: "UPLATE_REQUEST"
    }
};

export const uplate_success = (data) => {
    return {
        type: "UPLATE_SUCCESS",
        data
    }
};

export const uplate_error = (error) => {
    return {
        type: "UPLATE_ERROR",
        error
    }
};

export const getUplate = (userID) => {
    let config = {
        method: "GET",
        headers: {
            'Content-Type':'application/x-www-form-urlencoded'
        }
    };

    return dispatch => {
        dispatch(uplate_request());

        return fetch(`/getPayments?facebook_id=${userID}`, config)
            .then(response => response.json()
            .then(data => ({ data, response })))
            .then(({ data, response }) =>  {
                if (!response.ok) {
                    return dispatch(uplate_error(data.message.toString()));
                }

                dispatch(uplate_success(data))
            })
            .catch(err => console.log(err));
    }
};