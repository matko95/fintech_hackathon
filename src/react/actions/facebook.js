/*GET /v2.9/{user-id}/friends HTTP/1.1
 Host: graph.facebook.com*/


export const friends_data = (data) => {
    return {
        type: "FRIENDS_SUCCESS",
        data
    }
};

export const pay_id = (id) => {
    return {
        type: "PAY_ID_FOUND",
        id
    }
};

export const getFriends = (userID, token) => {
    let config = {
        method: "GET",
        headers: {
            'Content-Type':'application/x-www-form-urlencoded'
        }
    };

    return dispatch => {
        return fetch(`https://graph.facebook.com/v2.9/${userID}/taggable_friends?limit=15&access_token=${token}`, config)
            .then(response => response.json()
            .then(data => ({ data, response })))
            .then(({ data, response }) =>  {
                if (!response.ok) {
                    return;
                }

                dispatch(friends_data(data.data))
            })
            .catch(err => console.log(err));
    }
};

// search?type=user&q=ปาณิศา หลานตากอย.ตาแก้ว คงเพ็ชร&fields=id,name,picture
export const getTrueId = (name, token, url) => {
    let config = {
        method: "GET",
        headers: {
            'Content-Type':'application/x-www-form-urlencoded'
        }
    };

    return dispatch => {
        return fetch(`https://graph.facebook.com/v2.9/search?type=user&q=${name}&fields=id,name,picture&access_token=${token}`, config)
            .then(response => response.json()
            .then(data => ({ data, response })))
            .then(({ data, response }) =>  {
                if (!response.ok) {
                    return;
                }
                let searchData = data.data;
                for(let i=0; i < searchData.length; i++) {
                    if(searchData[i].picture.data.url === url){
                        console.log("FOUND A MATCH");
                        dispatch(pay_id(searchData[i].id));
                        return;
                    }
                }
            })
            .catch(err => console.log(err));
    }
};