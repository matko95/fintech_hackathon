export const pay_request = () => {
    return {
        type: 'PAY_REQUEST'
    }
};

export const pay_success = () => {
    return {
        type: 'PAY_SUCCESS'
    }
};

export const pay_error = () => {
    return {
        type: 'PAY_ERROR'
    }
};

export const sendPayment = (fromID, toID, amount, closeModal) => {
    let config = {
        method: "POST",
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify({
            from_facebook_id: fromID,
            to_facebook_id: toID,
            amount
        })
    };

    return dispatch => {
        dispatch(pay_request());

        return fetch(`/sendPayment`, config)
            .then(response => response.json()
            .then(data => ({ data, response })))
            .then(({ data, response }) =>  {
                if (!response.ok) {
                    return dispatch(pay_error(data.message.toString()));
                }

                if(data.status === 'waiting'){
                    FB.ui({
                        display: 'popup',
                        to: toID,
                        method: 'send',
                        link: `http://40e0f9fb.ngrok.io/transaction/${data.secret_id}`,
                    }, (response) => {
                        console.log(response);
                        closeModal();
                        dispatch(pay_success());
                    });
                    return
                }

                dispatch(pay_success());
                closeModal();
            })
            .catch(err => console.log(err));
    }
};