export const login_request = () => {
    return {
        type: "LOGIN_REQUEST",
        isFetching: true
    }
};

export const login_success = (fb_token, userID) => {
    return {
        type: "LOGIN_SUCCESS",
        isFetching: false,
        fb_token,
        userID
    }
};

export const login_error = (error) => {
    return {
        type: "LOGIN_ERROR",
        isFetching: false,
        error
    }
};

export const logout = () => {
    localStorage.clear();
    return {
        type: 'CLEAR_STORE'
    }
};