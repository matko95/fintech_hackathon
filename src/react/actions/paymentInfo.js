export const payment_info_request = () => {
    return {
        type: "PAYMENT_INFO_REQUEST",
        isFetching: true
    }
};

export const payment_info_success = (data) => {
    return {
        type: "PAYMENT_INFO_SUCCESS",
        isFetching: false,
        data
    }
};

export const payment_info_error = (error) => {
    return {
        type: "PAYMENT_INFO_ERROR",
        isFetching: false,
        error
    }
};

export const getPaymentInfo = (secretId) => {
    let config = {
        method: "GET",
        headers: {
            'Content-Type':'application/json'
        }
    };

    return dispatch => {
        dispatch(payment_info_request());

        return fetch(`/paymentInfo?secret_id=${secretId}`, config)
            .then(response => response.json()
            .then(data => ({ data, response })))
            .then(({ data, response }) =>  {
                if (!response.ok) {
                    return dispatch(payment_info_error(data.message.toString()));
                }

                dispatch(payment_info_success(data))
            })
            .catch(err => console.log(err));
    }
};

export const payment_success = () => {
    return {
        type: "PAYMENT_SUCCESS"
    }
};

export const executePayment = (secret_id, payment_type, account) => {
    let config = {
        method: "POST",
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify({
            secret_id,
            payment_type,
            account
        })
    };

    return dispatch => {
        return fetch(`/paymentResponse`, config)
            .then(response => response.json()
            .then(data => ({ data, response })))
            .then(({ data, response }) =>  {
                if (!response.ok) {
                    return console.log(data);
                }

                dispatch(payment_success())
            })
            .catch(err => console.log(err));
    }
};