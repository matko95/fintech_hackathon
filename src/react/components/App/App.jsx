import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as fbactions from '../../actions/facebook';
import * as payment from '../../actions/pay';
import Header from '../Header/';
import Friends from '../Friends/';

class App extends Component{
    constructor(props){
        super(props);

        this.state = {
        }
    }
    componentWillMount(){
        if(this.props.user.userID !== null && this.props.user.fb_token !== null){
            this.props.getFriends(this.props.user.userID, this.props.user.fb_token);
        }
    }
    render(){

        if(localStorage.getItem("accessToken") === null || localStorage.getItem("userID") === null){
            return (
                <Redirect to="/login"/>
            )
        }
        return(
            <div className="app">
                <Header>
                </Header>
                <Friends data={this.props.api} getTrueId={this.props.getTrueId} sendPayment={this.props.sendPayment}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => state;
const mapDispatchToProps = (dispatch) => bindActionCreators({...fbactions, ...payment}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);