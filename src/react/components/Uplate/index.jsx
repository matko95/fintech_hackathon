import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as fbactions from '../../actions/facebook';
import * as uplate from '../../actions/uplate';
import Header from '../Header/';
import Friends from '../Friends/';

class Uplate extends Component{
    constructor(props){
        super(props);

        this.state = {
        }
    }
    componentWillMount(){
        if(this.props.user.userID !== null && this.props.user.fb_token !== null){
            this.props.getUplate(this.props.user.userID);
        }
    }
    render(){

        if(localStorage.getItem("accessToken") === null || localStorage.getItem("userID") === null){
            return (
                <Redirect to="/login"/>
            )
        }
        return(
            <div className="app">
                <Header>
                </Header>
                <div className="friends">
                    {
                        this.props.uplate.uplate.length > 0 &&
                        <table className="table">
                            <tbody>
                            <tr>
                                <th></th>
                                <th>Ime uplatioca</th>
                                <th>Količina</th>
                                <th>Status uplate</th>
                            </tr>
                            {this.props.uplate.uplate.map((item, i) => (
                                <tr key={i}>
                                    <td>
                                        <img style={{borderRadius: "100%"}} src={item.receiverImage || item.senderImage} alt=""/>
                                        {
                                            !item.receive ?
                                                <i className="fa fa-arrow-up arrow"></i>
                                                :
                                                <i className="fa fa-arrow-down arrow"></i>
                                        }
                                    </td>
                                    <td>{item.receiverName || item.senderName}</td>
                                    <td>{item.amount && item.amount.toFixed(2)}</td>
                                    <td>{item.status}</td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => state;
const mapDispatchToProps = (dispatch) => bindActionCreators({...fbactions, ...uplate}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Uplate);