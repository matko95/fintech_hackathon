import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as paymentInfo from '../../actions/paymentInfo';
import Header from '../Header/';

class Transaction extends Component{
    constructor(props){
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);

        this.state = {
            placeholder: "Izaberite način uplate."
        }
    }
    componentWillMount(){
        const secretID = this.props.location.pathname.split("/").pop();
        console.log(this.props.location.pathname.split("/").pop());
        this.props.getPaymentInfo(secretID);
    }
    handleChange(e){
        const value = e.target.value;
        let placeholder = "";

        switch (value) {
            case "postnet":
                placeholder = "Unesite broj mobilnog telefona.";
                break;
            case "bank_account":
                placeholder = "Unesite broj računa.";
                break;
            case "bitcoin":
            case "ethereum":
                placeholder = "Unesite adresu walleta.";
                break;
            default:
                placeholder = "Izaberite način isplate.";
        }

        this.setState({
            placeholder
        })
    }
    handleFormSubmit(e){
        e.preventDefault();
        const secretID = this.props.location.pathname.split("/").pop();
        const payment_type = this.refs.payment_type.value;
        const account = this.refs.account.value;

        this.props.executePayment(secretID, payment_type, account);
    }
    render(){
        return(
            <div className="app">
                <Header>
                </Header>
                <div className="friends">
                    <img className="thumbnail" src={this.props.paymentInfo.paymentInfo.image} alt=""/>
                    <h1 className="title">Korisnik, {this.props.paymentInfo.paymentInfo.name} vam je poslao {this.props.paymentInfo.paymentInfo.amount.toFixed(2)} RSD.</h1>

                    <h3 className="title small-title">Izaberite način isplate</h3>
                    <form onSubmit={this.handleFormSubmit} className="classic-form">
                        <select name="payment_type" ref="payment_type" onChange={this.handleChange}>
                            <option disabled selected value>Izaberite način uplate</option>
                            <option value="postnet">Postnet</option>
                            <option value="bank_account">Uplata na račun</option>
                            <option value="bitcoin">Bitcoin</option>
                            <option value="ethereum">Ethereum</option>
                        </select>


                        <input name="account" ref="account" type="text" placeholder={this.state.placeholder}/>

                        <input onSubmit={this.handleFormSubmit} type="submit" value="Pošalji"/>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => state;
const mapDispatchToProps = (dispatch) => bindActionCreators(paymentInfo, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Transaction);