import React, { Component } from 'react';
import Modal from 'tg-modal';
import Isvg from 'react-inlinesvg';
import cash from '../../../assets/cash-payment.svg';

class Friends extends Component{
    constructor(props){
        super(props);

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.closeModal = this.closeModal.bind(this);

        this.state = {
            isOpen: false,
            toID: null
        }
    }
    closeModal(){
        this.setState({
            isOpen: false
        })
    }
    handleFormSubmit(e){
        e.preventDefault();
        let amount = this.refs.amount.value;
        this.props.sendPayment(localStorage.getItem("userID"), this.props.data.toID, amount, this.closeModal);
    }
    render(){
        return(
            <div className="friends">
                <h1 className="title">Pošalji novac direktno prijateljima</h1>
                {
                    this.props.data.friends.map((item, i) => (
                        <div className="friend" key={i}>
                            <img src={item.picture.data.url}/>
                            <p>{item.name}</p>
                            <span onClick={()=>{
                                this.setState({isOpen: true});
                                this.props.getTrueId(item.name, localStorage.getItem("accessToken"), item.picture.data.url);
                            }}>
                                <Isvg src={cash} style={{maxWidth: "200px", marginLeft: "auto"}}>
                                Here's some optional content for browsers that don't support XHR or inline
                                SVGs. You can use other React components here too. Here, I'll show you.
                            </Isvg>
                            </span>

                        </div>
                    ))
                }
                <Modal isOpen={this.state.isOpen} title="Pošalji novac" onCancel={()=> this.setState({isOpen: false})}>
                    <form className="form" onSubmit={this.handleFormSubmit}>
                        <label htmlFor="amount" className="amount">
                            <input type="number" ref="amount" placeholder="Unesi količinu" step="0.01"/>
                        </label>
                        <input onSubmit={this.handleFormSubmit} type="submit" value={"Pošalji novac"}/>
                    </form>
                </Modal>
            </div>
        );
    }
}

export default Friends;