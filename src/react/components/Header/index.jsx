import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logout } from '../../actions/login';
import Isvg from 'react-inlinesvg';
import logo from '../../../assets/logo-white.svg';

const Header = ({children, logout, isAuthenticated}) => (
    <header className="dashboard-header">
        <div className="content">
            <Link to="/">
                <Isvg src={logo} style={{maxWidth: "200px", marginLeft: "10%"}}>
                    Here's some optional content for browsers that don't support XHR or inline
                    SVGs. You can use other React components here too. Here, I'll show you.
                </Isvg>
            </Link>
            {children}
            {
                isAuthenticated &&
                <div className="logout">
                    <Link className="header-url" to={"/uplate"}>Pregled uplata</Link>
                    <span className="separator">|</span>
                    <span onClick={logout}>Logout</span>
                </div>
            }
        </div>
    </header>
);

const mapStateToProps = (state) => state.user;
const mapDispatchToProps = (dispatch) => bindActionCreators({ logout }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);