import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import Header from '../Header/';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../actions/login';
import { setFacebookToken } from '../../actions/setFacebookToken';
import FacebookLogin from 'react-facebook-login';

class Login extends Component{
    constructor(props){
        super(props);


        this.responseFacebook = this.responseFacebook.bind(this);
    }
    responseFacebook(response) {
        if(response){
            localStorage.setItem("accessToken", response.accessToken);
            localStorage.setItem("userID", response.userID);

            this.props.login_success(response.accessToken, response.userID);
            this.props.setFacebookToken(response.userID, response.accessToken, response.name, response.picture.data.url);
        }
        console.log(response);
    }
    render(){

        if(this.props.isAuthenticated){
            return (
                <Redirect to="/" push/>
            )
        }

        return(
            <div className="login">
                <Header/>
                <FacebookLogin
                    style={{textAlign:'center'}}
                    appId="286506968481680"
                    fields="name,email,picture"
                    scope="public_profile,user_friends,email,user_about_me,user_actions.books,user_actions.fitness,user_actions.music,user_actions.news,user_actions.video,user_birthday,user_education_history,user_events,user_games_activity,user_hometown,user_likes,user_location,user_managed_groups,user_photos,user_posts,user_relationships,user_relationship_details,user_religion_politics,user_tagged_places,user_videos,user_website,user_work_history,read_custom_friendlists,read_insights,read_audience_network_insights,read_page_mailboxes,manage_pages,publish_pages,publish_actions,rsvp_event,pages_show_list,pages_manage_cta,pages_manage_instant_articles,ads_read"
                    callback={this.responseFacebook}
                />
            </div>
        )
    }
}


const mapStateToProps = (state) => state.user;
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ ...actions, setFacebookToken}, dispatch)
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);