export const user = (state = {
    isFetching: false,
    fb_token: localStorage.getItem("accessToken"),
    isAuthenticated: localStorage.getItem("accessToken") !== null && localStorage.getItem("userID") !== null,
    loginError: "",
    userID: localStorage.getItem("userID")
}, action) => {
    switch(action.type){
        case 'LOGIN_REQUEST':
            return {
                ...state,
                isFetching: action.isFetching
            };
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                isFetching: action.isFetching,
                fb_token: action.fb_token,
                isAuthenticated: true,
                loginError: '',
                registerError: '',
                userID: action.userID
            };
        case 'LOGIN_ERROR':
            return {
                ...state,
                isFetching: action.isFetching,
                loginError: action.error
            };

        default:
            return state;
    }
};