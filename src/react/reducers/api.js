export const api = (state = {
    isFetching: false,
    friends: [],
    error: "",
    toID: null
}, action) => {
    switch(action.type){
        case 'UPLATE_SUCCESS':
            return {
                ...state,
                uplate: action.data
            };
        case 'PAY_ID_FOUND':
            return {
                ...state,
                toID: action.id
            };
        case 'FRIENDS_SUCCESS':
            return {
                ...state,
                isFetching: false,
                friends: action.data
            };
        case 'DATA_REQUEST':
            return {
                ...state,
                isFetching: action.isFetching
            };
        case 'DATA_SUCCESS':
            return {
                ...state,
                isFetching: action.isFetching,
                data: {
                    ...state.data,
                    [action.dataType]: action.data
                },
                error: ''
            };
        case 'DATA_ERROR':
            return {
                ...state,
                isFetching: action.isFetching,
                error: action.error
            };
        default:
            return state;
    }
};