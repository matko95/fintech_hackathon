export const uplate = (state = {
    isFetching: false,
    uplate: [],
}, action) => {
    switch(action.type) {
        case 'UPLATE_REQUEST':
            return {
                ...state,
                isFetching: true
            };
        case 'UPLATE_SUCCESS':
            return {
                ...state,
                uplate: action.data
            };
        case 'UPLATE_ERROR':
            return {
                ...state,
                isFetching: false
            };
        default:
            return state;
    }
};