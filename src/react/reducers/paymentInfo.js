export const paymentInfo = (state = {
    isFetching: false,
    paymentInfo: {
        name: "",
        image: "",
        amount: 0
    },
}, action) => {
    switch(action.type) {
        case 'PAYMENT_INFO_REQUEST':
            return {
                ...state,
                isFetching: true
            };
        case 'PAYMENT_INFO_SUCCESS':
            return {
                ...state,
                paymentInfo: action.data
            };
        case 'PAYMENT_INFO_ERROR':
            return {
                ...state,
                isFetching: false
            };
        default:
            return state;
    }
};